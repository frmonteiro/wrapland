/********************************************************************
Copyright 2014  Martin Gräßlin <mgraesslin@kde.org>

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) version 3, or any
later version accepted by the membership of KDE e.V. (or its
successor approved by the membership of KDE e.V.), which shall
act as a proxy defined in Section 6 of version 3 of the license.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library.  If not, see <http://www.gnu.org/licenses/>.
*********************************************************************/
// Qt
#include <QtTest>
// KWin
#include "../../src/client/compositor.h"
#include "../../src/client/connection_thread.h"
#include "../../src/client/event_queue.h"
#include "../../src/client/region.h"
#include "../../src/client/registry.h"
#include "../../src/client/shm_pool.h"
#include "../../src/client/subcompositor.h"
#include "../../src/client/subsurface.h"
#include "../../src/client/surface.h"
#include "../../src/server/buffer_interface.h"
#include "../../src/server/display.h"
#include "../../src/server/compositor_interface.h"
#include "../../src/server/subcompositor_interface.h"
#include "../../src/server/surface_interface.h"
// Wayland
#include <wayland-client.h>

class TestSubSurface : public QObject
{
    Q_OBJECT
public:
    explicit TestSubSurface(QObject *parent = nullptr);
private Q_SLOTS:
    void init();
    void cleanup();

    void testCreate();
    void testMode();
    void testPosition();
    void testPlaceAbove();
    void testPlaceBelow();
    void testDestroy();
    void testCast();
    void testSyncMode();
    void testDeSyncMode();
    void testMainSurfaceFromTree();
    void testRemoveSurface();
    void testMappingOfSurfaceTree();
    void testSurfaceAt();
    void testDestroyAttachedBuffer();
    void testDestroyParentSurface();

private:
    Wrapland::Server::Display *m_display;
    Wrapland::Server::CompositorInterface *m_compositorInterface;
    Wrapland::Server::SubCompositorInterface *m_subcompositorInterface;
    Wrapland::Client::ConnectionThread *m_connection;
    Wrapland::Client::Compositor *m_compositor;
    Wrapland::Client::ShmPool *m_shm;
    Wrapland::Client::SubCompositor *m_subCompositor;
    Wrapland::Client::EventQueue *m_queue;
    QThread *m_thread;
};

static const QString s_socketName = QStringLiteral("wrapland-test-wayland-subsurface-0");

TestSubSurface::TestSubSurface(QObject *parent)
    : QObject(parent)
    , m_display(nullptr)
    , m_compositorInterface(nullptr)
    , m_subcompositorInterface(nullptr)
    , m_connection(nullptr)
    , m_compositor(nullptr)
    , m_shm(nullptr)
    , m_subCompositor(nullptr)
    , m_queue(nullptr)
    , m_thread(nullptr)
{
}

void TestSubSurface::init()
{
    using namespace Wrapland::Server;
    delete m_display;
    m_display = new Display(this);
    m_display->setSocketName(s_socketName);
    m_display->start();
    QVERIFY(m_display->isRunning());
    m_display->createShm();

    // setup connection
    m_connection = new Wrapland::Client::ConnectionThread;
    QSignalSpy connectedSpy(m_connection, &Wrapland::Client::ConnectionThread::establishedChanged);
    m_connection->setSocketName(s_socketName);

    m_thread = new QThread(this);
    m_connection->moveToThread(m_thread);
    m_thread->start();

    m_connection->establishConnection();
    QVERIFY(connectedSpy.count() || connectedSpy.wait());
    QCOMPARE(connectedSpy.count(), 1);

    m_queue = new Wrapland::Client::EventQueue(this);
    QVERIFY(!m_queue->isValid());
    m_queue->setup(m_connection);
    QVERIFY(m_queue->isValid());

    Wrapland::Client::Registry registry;
    QSignalSpy compositorSpy(&registry, SIGNAL(compositorAnnounced(quint32,quint32)));
    QVERIFY(compositorSpy.isValid());
    QSignalSpy subCompositorSpy(&registry, SIGNAL(subCompositorAnnounced(quint32,quint32)));
    QVERIFY(subCompositorSpy.isValid());
    QVERIFY(!registry.eventQueue());
    registry.setEventQueue(m_queue);
    QCOMPARE(registry.eventQueue(), m_queue);
    registry.create(m_connection->display());
    QVERIFY(registry.isValid());
    registry.setup();

    m_compositorInterface = m_display->createCompositor(m_display);
    m_compositorInterface->create();
    QVERIFY(m_compositorInterface->isValid());

    m_subcompositorInterface = m_display->createSubCompositor(m_display);
    QVERIFY(m_subcompositorInterface);
    m_subcompositorInterface->create();
    QVERIFY(m_subcompositorInterface->isValid());

    QVERIFY(subCompositorSpy.wait());
    m_subCompositor = registry.createSubCompositor(subCompositorSpy.first().first().value<quint32>(), subCompositorSpy.first().last().value<quint32>(), this);

    if (compositorSpy.isEmpty()) {
        QVERIFY(compositorSpy.wait());
    }
    m_compositor = registry.createCompositor(compositorSpy.first().first().value<quint32>(), compositorSpy.first().last().value<quint32>(), this);

    m_shm = registry.createShmPool(registry.interface(Wrapland::Client::Registry::Interface::Shm).name, registry.interface(Wrapland::Client::Registry::Interface::Shm).version, this);
    QVERIFY(m_shm->isValid());
}

void TestSubSurface::cleanup()
{
    if (m_shm) {
        delete m_shm;
        m_shm = nullptr;
    }
    if (m_subCompositor) {
        delete m_subCompositor;
        m_subCompositor = nullptr;
    }
    if (m_compositor) {
        delete m_compositor;
        m_compositor = nullptr;
    }
    if (m_queue) {
        delete m_queue;
        m_queue = nullptr;
    }
    if (m_thread) {
        m_thread->quit();
        m_thread->wait();
        delete m_thread;
        m_thread = nullptr;
    }
    delete m_connection;
    m_connection = nullptr;

    delete m_display;
    m_display = nullptr;
}

void TestSubSurface::testCreate()
{
    using namespace Wrapland::Client;
    using namespace Wrapland::Server;
    QSignalSpy surfaceCreatedSpy(m_compositorInterface, SIGNAL(surfaceCreated(Wrapland::Server::SurfaceInterface*)));
    QVERIFY(surfaceCreatedSpy.isValid());

    // create two Surfaces
    std::unique_ptr<Surface> surface(m_compositor->createSurface());
    QVERIFY(surfaceCreatedSpy.wait());
    SurfaceInterface *serverSurface = surfaceCreatedSpy.first().first().value<Wrapland::Server::SurfaceInterface*>();
    QVERIFY(serverSurface);

    surfaceCreatedSpy.clear();
    std::unique_ptr<Surface> parent(m_compositor->createSurface());
    QVERIFY(surfaceCreatedSpy.wait());
    SurfaceInterface *serverParentSurface = surfaceCreatedSpy.first().first().value<Wrapland::Server::SurfaceInterface*>();
    QVERIFY(serverParentSurface);

    QSignalSpy subSurfaceCreatedSpy(m_subcompositorInterface, SIGNAL(subSurfaceCreated(Wrapland::Server::SubSurfaceInterface*)));
    QVERIFY(subSurfaceCreatedSpy.isValid());

    // create subSurface for surface of parent
    std::unique_ptr<SubSurface> subSurface(m_subCompositor->createSubSurface(QPointer<Surface>(surface.get()), QPointer<Surface>(parent.get())));

    QVERIFY(subSurfaceCreatedSpy.wait());
    SubSurfaceInterface *serverSubSurface = subSurfaceCreatedSpy.first().first().value<Wrapland::Server::SubSurfaceInterface*>();
    QVERIFY(serverSubSurface);
    QVERIFY(serverSubSurface->parentSurface());
    QCOMPARE(serverSubSurface->parentSurface().data(), serverParentSurface);
    QCOMPARE(serverSubSurface->surface().data(), serverSurface);
    QCOMPARE(serverSurface->subSurface().data(), serverSubSurface);
    QCOMPARE(serverSubSurface->mainSurface().data(), serverParentSurface);
    // children are only added after committing the surface
    QEXPECT_FAIL("", "Incorrect adding of child windows to workaround QtWayland behavior", Continue);
    QCOMPARE(serverParentSurface->childSubSurfaces().count(), 0);
    // so let's commit the surface, to apply the stacking change
    parent->commit(Surface::CommitFlag::None);
    wl_display_flush(m_connection->display());
    QCoreApplication::processEvents();
    QCOMPARE(serverParentSurface->childSubSurfaces().count(), 1);
    QCOMPARE(serverParentSurface->childSubSurfaces().first().data(), serverSubSurface);

    // and let's destroy it again
    QSignalSpy destroyedSpy(serverSubSurface, SIGNAL(destroyed(QObject*)));
    QVERIFY(destroyedSpy.isValid());
    subSurface.reset();
    QVERIFY(destroyedSpy.wait());
    QCOMPARE(serverSurface->subSurface(), QPointer<SubSurfaceInterface>());
    // only applied after next commit
    QEXPECT_FAIL("", "Incorrect removing of child windows to workaround QtWayland behavior", Continue);
    QCOMPARE(serverParentSurface->childSubSurfaces().count(), 1);
    // but the surface should be invalid
    if (!serverParentSurface->childSubSurfaces().isEmpty()) {
        QVERIFY(serverParentSurface->childSubSurfaces().first().isNull());
    }
    // committing the state should solve it
    parent->commit(Surface::CommitFlag::None);
    wl_display_flush(m_connection->display());
    QCoreApplication::processEvents();
    QCOMPARE(serverParentSurface->childSubSurfaces().count(), 0);
}

void TestSubSurface::testMode()
{
    using namespace Wrapland::Client;
    using namespace Wrapland::Server;
    // create two Surface
    std::unique_ptr<Surface> surface(m_compositor->createSurface());
    std::unique_ptr<Surface> parent(m_compositor->createSurface());

    QSignalSpy subSurfaceCreatedSpy(m_subcompositorInterface, SIGNAL(subSurfaceCreated(Wrapland::Server::SubSurfaceInterface*)));
    QVERIFY(subSurfaceCreatedSpy.isValid());

    // create the SubSurface for surface of parent
    std::unique_ptr<SubSurface> subSurface(m_subCompositor->createSubSurface(QPointer<Surface>(surface.get()), QPointer<Surface>(parent.get())));
    QVERIFY(subSurfaceCreatedSpy.wait());
    SubSurfaceInterface *serverSubSurface = subSurfaceCreatedSpy.first().first().value<Wrapland::Server::SubSurfaceInterface*>();
    QVERIFY(serverSubSurface);

    // both client and server subsurface should be in synchronized mode
    QCOMPARE(subSurface->mode(), SubSurface::Mode::Synchronized);
    QCOMPARE(serverSubSurface->mode(), SubSurfaceInterface::Mode::Synchronized);

    // verify that we can change to desynchronized
    QSignalSpy modeChangedSpy(serverSubSurface, SIGNAL(modeChanged(Wrapland::Server::SubSurfaceInterface::Mode)));
    QVERIFY(modeChangedSpy.isValid());

    subSurface->setMode(SubSurface::Mode::Desynchronized);
    QCOMPARE(subSurface->mode(), SubSurface::Mode::Desynchronized);

    QVERIFY(modeChangedSpy.wait());
    QCOMPARE(modeChangedSpy.first().first().value<Wrapland::Server::SubSurfaceInterface::Mode>(), SubSurfaceInterface::Mode::Desynchronized);
    QCOMPARE(serverSubSurface->mode(), SubSurfaceInterface::Mode::Desynchronized);

    // setting the same again won't change
    subSurface->setMode(SubSurface::Mode::Desynchronized);
    QCOMPARE(subSurface->mode(), SubSurface::Mode::Desynchronized);
    // not testing the signal, we do that after changing to synchronized

    // and change back to synchronized
    subSurface->setMode(SubSurface::Mode::Synchronized);
    QCOMPARE(subSurface->mode(), SubSurface::Mode::Synchronized);

    QVERIFY(modeChangedSpy.wait());
    QCOMPARE(modeChangedSpy.count(), 2);
    QCOMPARE(modeChangedSpy.first().first().value<Wrapland::Server::SubSurfaceInterface::Mode>(), SubSurfaceInterface::Mode::Desynchronized);
    QCOMPARE(modeChangedSpy.last().first().value<Wrapland::Server::SubSurfaceInterface::Mode>(), SubSurfaceInterface::Mode::Synchronized);
    QCOMPARE(serverSubSurface->mode(), SubSurfaceInterface::Mode::Synchronized);
}

void TestSubSurface::testPosition()
{
    using namespace Wrapland::Client;
    using namespace Wrapland::Server;
    // create two Surface
    std::unique_ptr<Surface> surface(m_compositor->createSurface());
    std::unique_ptr<Surface> parent(m_compositor->createSurface());

    QSignalSpy subSurfaceCreatedSpy(m_subcompositorInterface, SIGNAL(subSurfaceCreated(Wrapland::Server::SubSurfaceInterface*)));
    QVERIFY(subSurfaceCreatedSpy.isValid());

    // create the SubSurface for surface of parent
    std::unique_ptr<SubSurface> subSurface(m_subCompositor->createSubSurface(QPointer<Surface>(surface.get()), QPointer<Surface>(parent.get())));
    QVERIFY(subSurfaceCreatedSpy.wait());
    SubSurfaceInterface *serverSubSurface = subSurfaceCreatedSpy.first().first().value<Wrapland::Server::SubSurfaceInterface*>();
    QVERIFY(serverSubSurface);

    // create a signalspy
    QSignalSpy subsurfaceTreeChanged(serverSubSurface->parentSurface().data(), &SurfaceInterface::subSurfaceTreeChanged);
    QVERIFY(subsurfaceTreeChanged.isValid());

    // both client and server should have a default position
    QCOMPARE(subSurface->position(), QPoint());
    QCOMPARE(serverSubSurface->position(), QPoint());

    QSignalSpy positionChangedSpy(serverSubSurface, SIGNAL(positionChanged(QPoint)));
    QVERIFY(positionChangedSpy.isValid());

    // changing the position should not trigger a direct update on server side
    subSurface->setPosition(QPoint(10, 20));
    QCOMPARE(subSurface->position(), QPoint(10, 20));
    // ensure it's processed on server side
    wl_display_flush(m_connection->display());
    QCoreApplication::processEvents();
    QCOMPARE(serverSubSurface->position(), QPoint());
    // changing once more
    subSurface->setPosition(QPoint(20, 30));
    QCOMPARE(subSurface->position(), QPoint(20, 30));
    // ensure it's processed on server side
    wl_display_flush(m_connection->display());
    QCoreApplication::processEvents();
    QCOMPARE(serverSubSurface->position(), QPoint());

    // committing the parent surface should update the position
    parent->commit(Surface::CommitFlag::None);
    QCOMPARE(subsurfaceTreeChanged.count(), 0);
    QVERIFY(positionChangedSpy.wait());
    QCOMPARE(positionChangedSpy.count(), 1);
    QCOMPARE(positionChangedSpy.first().first().toPoint(), QPoint(20, 30));
    QCOMPARE(serverSubSurface->position(), QPoint(20, 30));
    QCOMPARE(subsurfaceTreeChanged.count(), 1);
}

void TestSubSurface::testPlaceAbove()
{
    using namespace Wrapland::Client;
    using namespace Wrapland::Server;
    // create needed Surfaces (one parent, three client
    std::unique_ptr<Surface> surface1(m_compositor->createSurface());
    std::unique_ptr<Surface> surface2(m_compositor->createSurface());
    std::unique_ptr<Surface> surface3(m_compositor->createSurface());
    std::unique_ptr<Surface> parent(m_compositor->createSurface());

    QSignalSpy subSurfaceCreatedSpy(m_subcompositorInterface, SIGNAL(subSurfaceCreated(Wrapland::Server::SubSurfaceInterface*)));
    QVERIFY(subSurfaceCreatedSpy.isValid());

    // create the SubSurfaces for surface of parent
    std::unique_ptr<SubSurface> subSurface1(m_subCompositor->createSubSurface(QPointer<Surface>(surface1.get()), QPointer<Surface>(parent.get())));
    QVERIFY(subSurfaceCreatedSpy.wait());
    SubSurfaceInterface *serverSubSurface1 = subSurfaceCreatedSpy.first().first().value<Wrapland::Server::SubSurfaceInterface*>();
    QVERIFY(serverSubSurface1);
    subSurfaceCreatedSpy.clear();
    std::unique_ptr<SubSurface> subSurface2(m_subCompositor->createSubSurface(QPointer<Surface>(surface2.get()), QPointer<Surface>(parent.get())));
    QVERIFY(subSurfaceCreatedSpy.wait());
    SubSurfaceInterface *serverSubSurface2 = subSurfaceCreatedSpy.first().first().value<Wrapland::Server::SubSurfaceInterface*>();
    QVERIFY(serverSubSurface2);
    subSurfaceCreatedSpy.clear();
    std::unique_ptr<SubSurface> subSurface3(m_subCompositor->createSubSurface(QPointer<Surface>(surface3.get()), QPointer<Surface>(parent.get())));
    QVERIFY(subSurfaceCreatedSpy.wait());
    SubSurfaceInterface *serverSubSurface3 = subSurfaceCreatedSpy.first().first().value<Wrapland::Server::SubSurfaceInterface*>();
    QVERIFY(serverSubSurface3);
    subSurfaceCreatedSpy.clear();

    // so far the stacking order should still be empty
    QEXPECT_FAIL("", "Incorrect adding of child windows to workaround QtWayland behavior", Continue);
    QVERIFY(serverSubSurface1->parentSurface()->childSubSurfaces().isEmpty());

    // committing the parent should create the stacking order
    parent->commit(Surface::CommitFlag::None);
    // ensure it's processed on server side
    wl_display_flush(m_connection->display());
    QCoreApplication::processEvents();
    QCOMPARE(serverSubSurface1->parentSurface()->childSubSurfaces().count(), 3);
    QCOMPARE(serverSubSurface1->parentSurface()->childSubSurfaces().at(0).data(), serverSubSurface1);
    QCOMPARE(serverSubSurface1->parentSurface()->childSubSurfaces().at(1).data(), serverSubSurface2);
    QCOMPARE(serverSubSurface1->parentSurface()->childSubSurfaces().at(2).data(), serverSubSurface3);

    // raising subsurface1 should place it to top of stack
    subSurface1->raise();
    // ensure it's processed on server side
    wl_display_flush(m_connection->display());
    QCoreApplication::processEvents();
    // but as long as parent is not committed it shouldn't change on server side
    QCOMPARE(serverSubSurface1->parentSurface()->childSubSurfaces().at(0).data(), serverSubSurface1);
    // after commit it's changed
    parent->commit(Surface::CommitFlag::None);
    wl_display_flush(m_connection->display());
    QCoreApplication::processEvents();
    QCOMPARE(serverSubSurface1->parentSurface()->childSubSurfaces().count(), 3);
    QCOMPARE(serverSubSurface1->parentSurface()->childSubSurfaces().at(0).data(), serverSubSurface2);
    QCOMPARE(serverSubSurface1->parentSurface()->childSubSurfaces().at(1).data(), serverSubSurface3);
    QCOMPARE(serverSubSurface1->parentSurface()->childSubSurfaces().at(2).data(), serverSubSurface1);

    // try placing 3 above 1, should result in 2, 1, 3
    subSurface3->placeAbove(QPointer<SubSurface>(subSurface1.get()));
    parent->commit(Surface::CommitFlag::None);
    wl_display_flush(m_connection->display());
    QCoreApplication::processEvents();
    QCOMPARE(serverSubSurface1->parentSurface()->childSubSurfaces().count(), 3);
    QCOMPARE(serverSubSurface1->parentSurface()->childSubSurfaces().at(0).data(), serverSubSurface2);
    QCOMPARE(serverSubSurface1->parentSurface()->childSubSurfaces().at(1).data(), serverSubSurface1);
    QCOMPARE(serverSubSurface1->parentSurface()->childSubSurfaces().at(2).data(), serverSubSurface3);

    // try placing 3 above 2, should result in 2, 3, 1
    subSurface3->placeAbove(QPointer<SubSurface>(subSurface2.get()));
    parent->commit(Surface::CommitFlag::None);
    wl_display_flush(m_connection->display());
    QCoreApplication::processEvents();
    QCOMPARE(serverSubSurface1->parentSurface()->childSubSurfaces().count(), 3);
    QCOMPARE(serverSubSurface1->parentSurface()->childSubSurfaces().at(0).data(), serverSubSurface2);
    QCOMPARE(serverSubSurface1->parentSurface()->childSubSurfaces().at(1).data(), serverSubSurface3);
    QCOMPARE(serverSubSurface1->parentSurface()->childSubSurfaces().at(2).data(), serverSubSurface1);

    // try placing 1 above 3 - shouldn't change
    subSurface1->placeAbove(QPointer<SubSurface>(subSurface3.get()));
    parent->commit(Surface::CommitFlag::None);
    wl_display_flush(m_connection->display());
    QCoreApplication::processEvents();
    QCOMPARE(serverSubSurface1->parentSurface()->childSubSurfaces().count(), 3);
    QCOMPARE(serverSubSurface1->parentSurface()->childSubSurfaces().at(0).data(), serverSubSurface2);
    QCOMPARE(serverSubSurface1->parentSurface()->childSubSurfaces().at(1).data(), serverSubSurface3);
    QCOMPARE(serverSubSurface1->parentSurface()->childSubSurfaces().at(2).data(), serverSubSurface1);

    // and 2 above 3 - > 3, 2, 1
    subSurface2->placeAbove(QPointer<SubSurface>(subSurface3.get()));
    parent->commit(Surface::CommitFlag::None);
    wl_display_flush(m_connection->display());
    QCoreApplication::processEvents();
    QCOMPARE(serverSubSurface1->parentSurface()->childSubSurfaces().count(), 3);
    QCOMPARE(serverSubSurface1->parentSurface()->childSubSurfaces().at(0).data(), serverSubSurface3);
    QCOMPARE(serverSubSurface1->parentSurface()->childSubSurfaces().at(1).data(), serverSubSurface2);
    QCOMPARE(serverSubSurface1->parentSurface()->childSubSurfaces().at(2).data(), serverSubSurface1);
}

void TestSubSurface::testPlaceBelow()
{
    using namespace Wrapland::Client;
    using namespace Wrapland::Server;
    // create needed Surfaces (one parent, three client
    std::unique_ptr<Surface> surface1(m_compositor->createSurface());
    std::unique_ptr<Surface> surface2(m_compositor->createSurface());
    std::unique_ptr<Surface> surface3(m_compositor->createSurface());
    std::unique_ptr<Surface> parent(m_compositor->createSurface());

    QSignalSpy subSurfaceCreatedSpy(m_subcompositorInterface, SIGNAL(subSurfaceCreated(Wrapland::Server::SubSurfaceInterface*)));
    QVERIFY(subSurfaceCreatedSpy.isValid());

    // create the SubSurfaces for surface of parent
    std::unique_ptr<SubSurface> subSurface1(m_subCompositor->createSubSurface(QPointer<Surface>(surface1.get()), QPointer<Surface>(parent.get())));
    QVERIFY(subSurfaceCreatedSpy.wait());
    SubSurfaceInterface *serverSubSurface1 = subSurfaceCreatedSpy.first().first().value<Wrapland::Server::SubSurfaceInterface*>();
    QVERIFY(serverSubSurface1);
    subSurfaceCreatedSpy.clear();
    std::unique_ptr<SubSurface> subSurface2(m_subCompositor->createSubSurface(QPointer<Surface>(surface2.get()), QPointer<Surface>(parent.get())));
    QVERIFY(subSurfaceCreatedSpy.wait());
    SubSurfaceInterface *serverSubSurface2 = subSurfaceCreatedSpy.first().first().value<Wrapland::Server::SubSurfaceInterface*>();
    QVERIFY(serverSubSurface2);
    subSurfaceCreatedSpy.clear();
    std::unique_ptr<SubSurface> subSurface3(m_subCompositor->createSubSurface(QPointer<Surface>(surface3.get()), QPointer<Surface>(parent.get())));
    QVERIFY(subSurfaceCreatedSpy.wait());
    SubSurfaceInterface *serverSubSurface3 = subSurfaceCreatedSpy.first().first().value<Wrapland::Server::SubSurfaceInterface*>();
    QVERIFY(serverSubSurface3);
    subSurfaceCreatedSpy.clear();

    // so far the stacking order should still be empty
    QEXPECT_FAIL("", "Incorrect adding of child windows to workaround QtWayland behavior", Continue);
    QVERIFY(serverSubSurface1->parentSurface()->childSubSurfaces().isEmpty());

    // committing the parent should create the stacking order
    parent->commit(Surface::CommitFlag::None);
    // ensure it's processed on server side
    wl_display_flush(m_connection->display());
    QCoreApplication::processEvents();
    QCOMPARE(serverSubSurface1->parentSurface()->childSubSurfaces().count(), 3);
    QCOMPARE(serverSubSurface1->parentSurface()->childSubSurfaces().at(0).data(), serverSubSurface1);
    QCOMPARE(serverSubSurface1->parentSurface()->childSubSurfaces().at(1).data(), serverSubSurface2);
    QCOMPARE(serverSubSurface1->parentSurface()->childSubSurfaces().at(2).data(), serverSubSurface3);

    // lowering subsurface3 should place it to the bottom of stack
    subSurface3->lower();
    // ensure it's processed on server side
    wl_display_flush(m_connection->display());
    QCoreApplication::processEvents();
    // but as long as parent is not committed it shouldn't change on server side
    QCOMPARE(serverSubSurface1->parentSurface()->childSubSurfaces().at(0).data(), serverSubSurface1);
    // after commit it's changed
    parent->commit(Surface::CommitFlag::None);
    wl_display_flush(m_connection->display());
    QCoreApplication::processEvents();
    QCOMPARE(serverSubSurface1->parentSurface()->childSubSurfaces().count(), 3);
    QCOMPARE(serverSubSurface1->parentSurface()->childSubSurfaces().at(0).data(), serverSubSurface3);
    QCOMPARE(serverSubSurface1->parentSurface()->childSubSurfaces().at(1).data(), serverSubSurface1);
    QCOMPARE(serverSubSurface1->parentSurface()->childSubSurfaces().at(2).data(), serverSubSurface2);

    // place 1 below 3 -> 1, 3, 2
    subSurface1->placeBelow(QPointer<SubSurface>(subSurface3.get()));
    parent->commit(Surface::CommitFlag::None);
    wl_display_flush(m_connection->display());
    QCoreApplication::processEvents();
    QCOMPARE(serverSubSurface1->parentSurface()->childSubSurfaces().count(), 3);
    QCOMPARE(serverSubSurface1->parentSurface()->childSubSurfaces().at(0).data(), serverSubSurface1);
    QCOMPARE(serverSubSurface1->parentSurface()->childSubSurfaces().at(1).data(), serverSubSurface3);
    QCOMPARE(serverSubSurface1->parentSurface()->childSubSurfaces().at(2).data(), serverSubSurface2);

    // 2 below 3 -> 1, 2, 3
    subSurface2->placeBelow(QPointer<SubSurface>(subSurface3.get()));
    parent->commit(Surface::CommitFlag::None);
    wl_display_flush(m_connection->display());
    QCoreApplication::processEvents();
    QCOMPARE(serverSubSurface1->parentSurface()->childSubSurfaces().count(), 3);
    QCOMPARE(serverSubSurface1->parentSurface()->childSubSurfaces().at(0).data(), serverSubSurface1);
    QCOMPARE(serverSubSurface1->parentSurface()->childSubSurfaces().at(1).data(), serverSubSurface2);
    QCOMPARE(serverSubSurface1->parentSurface()->childSubSurfaces().at(2).data(), serverSubSurface3);

    // 1 below 2 -> shouldn't change
    subSurface1->placeBelow(QPointer<SubSurface>(subSurface2.get()));
    parent->commit(Surface::CommitFlag::None);
    wl_display_flush(m_connection->display());
    QCoreApplication::processEvents();
    QCOMPARE(serverSubSurface1->parentSurface()->childSubSurfaces().count(), 3);
    QCOMPARE(serverSubSurface1->parentSurface()->childSubSurfaces().at(0).data(), serverSubSurface1);
    QCOMPARE(serverSubSurface1->parentSurface()->childSubSurfaces().at(1).data(), serverSubSurface2);
    QCOMPARE(serverSubSurface1->parentSurface()->childSubSurfaces().at(2).data(), serverSubSurface3);

    // and 3 below 1 -> 3, 1, 2
    subSurface3->placeBelow(QPointer<SubSurface>(subSurface1.get()));
    parent->commit(Surface::CommitFlag::None);
    wl_display_flush(m_connection->display());
    QCoreApplication::processEvents();
    QCOMPARE(serverSubSurface1->parentSurface()->childSubSurfaces().count(), 3);
    QCOMPARE(serverSubSurface1->parentSurface()->childSubSurfaces().at(0).data(), serverSubSurface3);
    QCOMPARE(serverSubSurface1->parentSurface()->childSubSurfaces().at(1).data(), serverSubSurface1);
    QCOMPARE(serverSubSurface1->parentSurface()->childSubSurfaces().at(2).data(), serverSubSurface2);
}

void TestSubSurface::testDestroy()
{
    using namespace Wrapland::Client;

    // create two Surfaces
    std::unique_ptr<Surface> surface(m_compositor->createSurface());
    std::unique_ptr<Surface> parent(m_compositor->createSurface());
    // create subSurface for surface of parent
    std::unique_ptr<SubSurface> subSurface(m_subCompositor->createSubSurface(QPointer<Surface>(surface.get()), QPointer<Surface>(parent.get())));

    connect(m_connection, &ConnectionThread::establishedChanged, m_compositor, &Compositor::release);
    connect(m_connection, &ConnectionThread::establishedChanged, m_subCompositor, &SubCompositor::release);
    connect(m_connection, &ConnectionThread::establishedChanged, m_shm, &ShmPool::release);
    connect(m_connection, &ConnectionThread::establishedChanged, m_queue, &EventQueue::release);
    connect(m_connection, &ConnectionThread::establishedChanged, surface.get(), &Surface::release);
    connect(m_connection, &ConnectionThread::establishedChanged, parent.get(), &Surface::release);
    connect(m_connection, &ConnectionThread::establishedChanged, subSurface.get(), &SubSurface::release);
    QVERIFY(subSurface->isValid());

    delete m_display;
    m_display = nullptr;
    QTRY_VERIFY(!m_connection->established());

    // Now the pool should be destroyed.
    QTRY_VERIFY(!subSurface->isValid());

    // Calling destroy again should not fail.
    subSurface->release();
}

void TestSubSurface::testCast()
{
    using namespace Wrapland::Client;

    // create two Surfaces
    std::unique_ptr<Surface> surface(m_compositor->createSurface());
    std::unique_ptr<Surface> parent(m_compositor->createSurface());
    // create subSurface for surface of parent
    std::unique_ptr<SubSurface> subSurface(m_subCompositor->createSubSurface(QPointer<Surface>(surface.get()), QPointer<Surface>(parent.get())));

    QCOMPARE(SubSurface::get(*(subSurface.get())), QPointer<SubSurface>(subSurface.get()));
}

void TestSubSurface::testSyncMode()
{
    // this test verifies that state is only applied when the parent surface commits its pending state
    using namespace Wrapland::Client;
    using namespace Wrapland::Server;

    QSignalSpy surfaceCreatedSpy(m_compositorInterface, &CompositorInterface::surfaceCreated);
    QVERIFY(surfaceCreatedSpy.isValid());

    std::unique_ptr<Surface> surface(m_compositor->createSurface());
    QVERIFY(surfaceCreatedSpy.wait());
    auto childSurface = surfaceCreatedSpy.first().first().value<SurfaceInterface*>();
    QVERIFY(childSurface);

    std::unique_ptr<Surface> parent(m_compositor->createSurface());
    QVERIFY(surfaceCreatedSpy.wait());
    auto parentSurface = surfaceCreatedSpy.last().first().value<SurfaceInterface*>();
    QVERIFY(parentSurface);
    QSignalSpy subSurfaceTreeChangedSpy(parentSurface, &SurfaceInterface::subSurfaceTreeChanged);
    QVERIFY(subSurfaceTreeChangedSpy.isValid());
    // create subSurface for surface of parent
    std::unique_ptr<SubSurface> subSurface(m_subCompositor->createSubSurface(QPointer<Surface>(surface.get()), QPointer<Surface>(parent.get())));
    QVERIFY(subSurfaceTreeChangedSpy.wait());
    QCOMPARE(subSurfaceTreeChangedSpy.count(), 1);

    // let's damage the child surface
    QSignalSpy childDamagedSpy(childSurface, &SurfaceInterface::damaged);
    QVERIFY(childDamagedSpy.isValid());

    QImage image(QSize(200, 200), QImage::Format_ARGB32_Premultiplied);
    image.fill(Qt::black);
    surface->attachBuffer(m_shm->createBuffer(image));
    surface->damage(QRect(0, 0, 200, 200));
    surface->commit();

    // state should be applied when the parent surface's state gets applied
    QVERIFY(!childDamagedSpy.wait(100));
    QVERIFY(!childSurface->buffer());

    QVERIFY(!childSurface->isMapped());
    QVERIFY(!parentSurface->isMapped());

    QImage image2(QSize(400, 400), QImage::Format_ARGB32_Premultiplied);
    image2.fill(Qt::red);
    parent->attachBuffer(m_shm->createBuffer(image2));
    parent->damage(QRect(0, 0, 400, 400));
    parent->commit();
    QVERIFY(childDamagedSpy.wait());
    QCOMPARE(childDamagedSpy.count(), 1);
    QCOMPARE(subSurfaceTreeChangedSpy.count(), 2);
    QCOMPARE(childSurface->buffer()->data(), image);
    QCOMPARE(parentSurface->buffer()->data(), image2);
    QVERIFY(childSurface->isMapped());
    QVERIFY(parentSurface->isMapped());

    // sending frame rendered to parent should also send it to child
    QSignalSpy frameRenderedSpy(surface.get(), &Surface::frameRendered);
    QVERIFY(frameRenderedSpy.isValid());
    parentSurface->frameRendered(100);
    QVERIFY(frameRenderedSpy.wait());
}

void TestSubSurface::testDeSyncMode()
{
    // this test verifies that state gets applied immediately in desync mode
    using namespace Wrapland::Client;
    using namespace Wrapland::Server;

    QSignalSpy surfaceCreatedSpy(m_compositorInterface, &CompositorInterface::surfaceCreated);
    QVERIFY(surfaceCreatedSpy.isValid());

    std::unique_ptr<Surface> surface(m_compositor->createSurface());
    QVERIFY(surfaceCreatedSpy.wait());
    auto childSurface = surfaceCreatedSpy.first().first().value<SurfaceInterface*>();
    QVERIFY(childSurface);

    std::unique_ptr<Surface> parent(m_compositor->createSurface());
    QVERIFY(surfaceCreatedSpy.wait());
    auto parentSurface = surfaceCreatedSpy.last().first().value<SurfaceInterface*>();
    QVERIFY(parentSurface);
    QSignalSpy subSurfaceTreeChangedSpy(parentSurface, &SurfaceInterface::subSurfaceTreeChanged);
    QVERIFY(subSurfaceTreeChangedSpy.isValid());
    // create subSurface for surface of parent
    std::unique_ptr<SubSurface> subSurface(m_subCompositor->createSubSurface(QPointer<Surface>(surface.get()), QPointer<Surface>(parent.get())));
    QVERIFY(subSurfaceTreeChangedSpy.wait());
    QCOMPARE(subSurfaceTreeChangedSpy.count(), 1);

    // let's damage the child surface
    QSignalSpy childDamagedSpy(childSurface, &SurfaceInterface::damaged);
    QVERIFY(childDamagedSpy.isValid());

    QImage image(QSize(200, 200), QImage::Format_ARGB32_Premultiplied);
    image.fill(Qt::black);
    surface->attachBuffer(m_shm->createBuffer(image));
    surface->damage(QRect(0, 0, 200, 200));
    surface->commit(Surface::CommitFlag::None);

    // state should be applied when the parent surface's state gets applied or when the subsurface switches to desync
    QVERIFY(!childDamagedSpy.wait(100));
    QVERIFY(!childSurface->isMapped());
    QVERIFY(!parentSurface->isMapped());

    // setting to desync should apply the state directly
    subSurface->setMode(SubSurface::Mode::Desynchronized);
    QVERIFY(childDamagedSpy.wait());
    QCOMPARE(subSurfaceTreeChangedSpy.count(), 2);
    QCOMPARE(childSurface->buffer()->data(), image);
    QVERIFY(!childSurface->isMapped());
    QVERIFY(!parentSurface->isMapped());

    // and damaging again, should directly be applied
    image.fill(Qt::red);
    surface->attachBuffer(m_shm->createBuffer(image));
    surface->damage(QRect(0, 0, 200, 200));
    surface->commit(Surface::CommitFlag::None);
    QVERIFY(childDamagedSpy.wait());
    QCOMPARE(subSurfaceTreeChangedSpy.count(), 3);
    QCOMPARE(childSurface->buffer()->data(), image);
}


void TestSubSurface::testMainSurfaceFromTree()
{
    // this test verifies that in a tree of surfaces every surface has the same main surface
    using namespace Wrapland::Client;
    using namespace Wrapland::Server;
    QSignalSpy surfaceCreatedSpy(m_compositorInterface, &CompositorInterface::surfaceCreated);
    QVERIFY(surfaceCreatedSpy.isValid());

    std::unique_ptr<Surface> parentSurface(m_compositor->createSurface());
    QVERIFY(surfaceCreatedSpy.wait());
    auto parentServerSurface = surfaceCreatedSpy.last().first().value<SurfaceInterface*>();
    QVERIFY(parentServerSurface);
    std::unique_ptr<Surface> childLevel1Surface(m_compositor->createSurface());
    QVERIFY(surfaceCreatedSpy.wait());
    auto childLevel1ServerSurface = surfaceCreatedSpy.last().first().value<SurfaceInterface*>();
    QVERIFY(childLevel1ServerSurface);
    std::unique_ptr<Surface> childLevel2Surface(m_compositor->createSurface());
    QVERIFY(surfaceCreatedSpy.wait());
    auto childLevel2ServerSurface = surfaceCreatedSpy.last().first().value<SurfaceInterface*>();
    QVERIFY(childLevel2ServerSurface);
    std::unique_ptr<Surface> childLevel3Surface(m_compositor->createSurface());
    QVERIFY(surfaceCreatedSpy.wait());
    auto childLevel3ServerSurface = surfaceCreatedSpy.last().first().value<SurfaceInterface*>();
    QVERIFY(childLevel3ServerSurface);

    QSignalSpy subSurfaceTreeChangedSpy(parentServerSurface, &SurfaceInterface::subSurfaceTreeChanged);
    QVERIFY(subSurfaceTreeChangedSpy.isValid());

    auto *sub1 = m_subCompositor->createSubSurface(childLevel1Surface.get(),
                                                   parentSurface.get());
    auto *sub2 = m_subCompositor->createSubSurface(childLevel2Surface.get(),
                                                   childLevel1Surface.get());
    auto *sub3 = m_subCompositor->createSubSurface(childLevel3Surface.get(),
                                                   childLevel2Surface.get());

    parentSurface->commit(Surface::CommitFlag::None);
    QVERIFY(subSurfaceTreeChangedSpy.wait());

    QCOMPARE(parentServerSurface->childSubSurfaces().count(), 1);
    auto child = parentServerSurface->childSubSurfaces().first();
    QCOMPARE(child->parentSurface().data(), parentServerSurface);
    QCOMPARE(child->mainSurface().data(), parentServerSurface);
    QCOMPARE(child->surface()->childSubSurfaces().count(), 1);
    auto child2 = child->surface()->childSubSurfaces().first();
    QCOMPARE(child2->parentSurface().data(), child->surface().data());
    QCOMPARE(child2->mainSurface().data(), parentServerSurface);
    QCOMPARE(child2->surface()->childSubSurfaces().count(), 1);
    auto child3 = child2->surface()->childSubSurfaces().first();
    QCOMPARE(child3->parentSurface().data(), child2->surface().data());
    QCOMPARE(child3->mainSurface().data(), parentServerSurface);
    QCOMPARE(child3->surface()->childSubSurfaces().count(), 0);

    delete sub1;
    delete sub2;
    delete sub3;
}

void TestSubSurface::testRemoveSurface()
{
    // this test verifies that removing the surface also removes the sub-surface from the parent
    using namespace Wrapland::Client;
    using namespace Wrapland::Server;

    QSignalSpy surfaceCreatedSpy(m_compositorInterface, &CompositorInterface::surfaceCreated);
    QVERIFY(surfaceCreatedSpy.isValid());

    std::unique_ptr<Surface> parentSurface(m_compositor->createSurface());
    QVERIFY(surfaceCreatedSpy.wait());
    auto parentServerSurface = surfaceCreatedSpy.last().first().value<SurfaceInterface*>();
    QVERIFY(parentServerSurface);
    std::unique_ptr<Surface> childSurface(m_compositor->createSurface());
    QVERIFY(surfaceCreatedSpy.wait());
    auto childServerSurface = surfaceCreatedSpy.last().first().value<SurfaceInterface*>();
    QVERIFY(childServerSurface);

    QSignalSpy subSurfaceTreeChangedSpy(parentServerSurface, &SurfaceInterface::subSurfaceTreeChanged);
    QVERIFY(subSurfaceTreeChangedSpy.isValid());

    std::unique_ptr<SubSurface> sub(m_subCompositor->createSubSurface(childSurface.get(),
                                                                     parentSurface.get()));
    parentSurface->commit(Surface::CommitFlag::None);
    QVERIFY(subSurfaceTreeChangedSpy.wait());

    QCOMPARE(parentServerSurface->childSubSurfaces().count(), 1);

    // destroy surface, takes place immediately
    childSurface.reset();
    QVERIFY(subSurfaceTreeChangedSpy.wait());
    QCOMPARE(parentServerSurface->childSubSurfaces().count(), 0);
}

void TestSubSurface::testMappingOfSurfaceTree()
{
    // this test verifies mapping and unmapping of a sub-surface tree
    using namespace Wrapland::Client;
    using namespace Wrapland::Server;
    QSignalSpy surfaceCreatedSpy(m_compositorInterface, &CompositorInterface::surfaceCreated);
    QVERIFY(surfaceCreatedSpy.isValid());

    std::unique_ptr<Surface> parentSurface(m_compositor->createSurface());
    QVERIFY(surfaceCreatedSpy.wait());
    auto parentServerSurface = surfaceCreatedSpy.last().first().value<SurfaceInterface*>();
    QVERIFY(parentServerSurface);
    std::unique_ptr<Surface> childLevel1Surface(m_compositor->createSurface());
    QVERIFY(surfaceCreatedSpy.wait());
    auto childLevel1ServerSurface = surfaceCreatedSpy.last().first().value<SurfaceInterface*>();
    QVERIFY(childLevel1ServerSurface);
    std::unique_ptr<Surface> childLevel2Surface(m_compositor->createSurface());
    QVERIFY(surfaceCreatedSpy.wait());
    auto childLevel2ServerSurface = surfaceCreatedSpy.last().first().value<SurfaceInterface*>();
    QVERIFY(childLevel2ServerSurface);
    std::unique_ptr<Surface> childLevel3Surface(m_compositor->createSurface());
    QVERIFY(surfaceCreatedSpy.wait());
    auto childLevel3ServerSurface = surfaceCreatedSpy.last().first().value<SurfaceInterface*>();
    QVERIFY(childLevel3ServerSurface);

    QSignalSpy subSurfaceTreeChangedSpy(parentServerSurface, &SurfaceInterface::subSurfaceTreeChanged);
    QVERIFY(subSurfaceTreeChangedSpy.isValid());

    auto subSurfaceLevel1 = m_subCompositor->createSubSurface(childLevel1Surface.get(), parentSurface.get());
    auto subSurfaceLevel2 = m_subCompositor->createSubSurface(childLevel2Surface.get(), childLevel1Surface.get());
    auto subSurfaceLevel3 = m_subCompositor->createSubSurface(childLevel3Surface.get(), childLevel2Surface.get());

    parentSurface->commit(Surface::CommitFlag::None);
    QVERIFY(subSurfaceTreeChangedSpy.wait());

    QCOMPARE(parentServerSurface->childSubSurfaces().count(), 1);
    auto child = parentServerSurface->childSubSurfaces().first();
    QCOMPARE(child->surface()->childSubSurfaces().count(), 1);
    auto child2 = child->surface()->childSubSurfaces().first();
    QCOMPARE(child2->surface()->childSubSurfaces().count(), 1);
    auto child3 = child2->surface()->childSubSurfaces().first();
    QCOMPARE(child3->parentSurface().data(), child2->surface().data());
    QCOMPARE(child3->mainSurface().data(), parentServerSurface);
    QCOMPARE(child3->surface()->childSubSurfaces().count(), 0);

    // so far no surface is mapped
    QVERIFY(!parentServerSurface->isMapped());
    QVERIFY(!child->surface()->isMapped());
    QVERIFY(!child2->surface()->isMapped());
    QVERIFY(!child3->surface()->isMapped());

    // first set all subsurfaces to desync, to simplify
    subSurfaceLevel1->setMode(SubSurface::Mode::Desynchronized);
    subSurfaceLevel2->setMode(SubSurface::Mode::Desynchronized);
    subSurfaceLevel3->setMode(SubSurface::Mode::Desynchronized);

    // first map the child, should not map it
    QSignalSpy child3DamageSpy(child3->surface().data(), &SurfaceInterface::damaged);
    QVERIFY(child3DamageSpy.isValid());
    QImage image(QSize(200, 200), QImage::Format_ARGB32_Premultiplied);
    image.fill(Qt::black);
    childLevel3Surface->attachBuffer(m_shm->createBuffer(image));
    childLevel3Surface->damage(QRect(0, 0, 200, 200));
    childLevel3Surface->commit(Surface::CommitFlag::None);
    QVERIFY(child3DamageSpy.wait());
    QVERIFY(child3->surface()->buffer());
    QVERIFY(!child3->surface()->isMapped());

    // let's map the top level
    QSignalSpy parentSpy(parentServerSurface, &SurfaceInterface::damaged);
    QVERIFY(parentSpy.isValid());
    parentSurface->attachBuffer(m_shm->createBuffer(image));
    parentSurface->damage(QRect(0, 0, 200, 200));
    parentSurface->commit(Surface::CommitFlag::None);
    QVERIFY(parentSpy.wait());
    QVERIFY(parentServerSurface->isMapped());
    // children should not yet be mapped
    QEXPECT_FAIL("", "Workaround for QtWayland bug https://bugreports.qt.io/browse/QTBUG-52192", Continue);
    QVERIFY(!child->surface()->isMapped());
    QEXPECT_FAIL("", "Workaround for QtWayland bug https://bugreports.qt.io/browse/QTBUG-52192", Continue);
    QVERIFY(!child2->surface()->isMapped());
    QEXPECT_FAIL("", "Workaround for QtWayland bug https://bugreports.qt.io/browse/QTBUG-52192", Continue);
    QVERIFY(!child3->surface()->isMapped());

    // next level
    QSignalSpy child2DamageSpy(child2->surface().data(), &SurfaceInterface::damaged);
    QVERIFY(child2DamageSpy.isValid());
    childLevel2Surface->attachBuffer(m_shm->createBuffer(image));
    childLevel2Surface->damage(QRect(0, 0, 200, 200));
    childLevel2Surface->commit(Surface::CommitFlag::None);
    QVERIFY(child2DamageSpy.wait());
    QVERIFY(parentServerSurface->isMapped());
    // children should not yet be mapped
    QEXPECT_FAIL("", "Workaround for QtWayland bug https://bugreports.qt.io/browse/QTBUG-52192", Continue);
    QVERIFY(!child->surface()->isMapped());
    QEXPECT_FAIL("", "Workaround for QtWayland bug https://bugreports.qt.io/browse/QTBUG-52192", Continue);
    QVERIFY(!child2->surface()->isMapped());
    QEXPECT_FAIL("", "Workaround for QtWayland bug https://bugreports.qt.io/browse/QTBUG-52192", Continue);
    QVERIFY(!child3->surface()->isMapped());

    // last but not least the first child level, which should map all our subsurfaces
    QSignalSpy child1DamageSpy(child->surface().data(), &SurfaceInterface::damaged);
    QVERIFY(child1DamageSpy.isValid());
    childLevel1Surface->attachBuffer(m_shm->createBuffer(image));
    childLevel1Surface->damage(QRect(0, 0, 200, 200));
    childLevel1Surface->commit(Surface::CommitFlag::None);
    QVERIFY(child1DamageSpy.wait());

    // everything is mapped
    QVERIFY(parentServerSurface->isMapped());
    QVERIFY(child->surface()->isMapped());
    QVERIFY(child2->surface()->isMapped());
    QVERIFY(child3->surface()->isMapped());

    // unmapping a parent should unmap the complete tree
    QSignalSpy unmappedSpy(child->surface().data(), &SurfaceInterface::unmapped);
    QVERIFY(unmappedSpy.isValid());
    childLevel1Surface->attachBuffer(Buffer::Ptr());
    childLevel1Surface->damage(QRect(0, 0, 200, 200));
    childLevel1Surface->commit(Surface::CommitFlag::None);
    QVERIFY(unmappedSpy.wait());

    QVERIFY(parentServerSurface->isMapped());
    QVERIFY(!child->surface()->isMapped());
    QVERIFY(!child2->surface()->isMapped());
    QVERIFY(!child3->surface()->isMapped());

    delete subSurfaceLevel1;
    delete subSurfaceLevel2;
    delete subSurfaceLevel3;
}

void TestSubSurface::testSurfaceAt()
{
    // this test verifies that the correct surface is picked in a sub-surface tree
    using namespace Wrapland::Client;
    using namespace Wrapland::Server;
    // first create a parent surface and map it
    QSignalSpy serverSurfaceCreated(m_compositorInterface, &CompositorInterface::surfaceCreated);
    QVERIFY(serverSurfaceCreated.isValid());
    std::unique_ptr<Surface> parent(m_compositor->createSurface());
    QImage image(QSize(100, 100), QImage::Format_ARGB32_Premultiplied);
    image.fill(Qt::red);
    parent->attachBuffer(m_shm->createBuffer(image));
    parent->damage(QRect(0, 0, 100, 100));
    parent->commit(Surface::CommitFlag::None);
    QVERIFY(serverSurfaceCreated.wait());
    SurfaceInterface *parentServerSurface = serverSurfaceCreated.last().first().value<Wrapland::Server::SurfaceInterface*>();

    // create two child sub surfaces, those won't be mapped, just added to the parent
    // this is to simulate the behavior of QtWayland
    std::unique_ptr<Surface> directChild1(m_compositor->createSurface());
    QVERIFY(serverSurfaceCreated.wait());
    SurfaceInterface *directChild1ServerSurface = serverSurfaceCreated.last().first().value<Wrapland::Server::SurfaceInterface*>();
    QVERIFY(directChild1ServerSurface);
    std::unique_ptr<Surface> directChild2(m_compositor->createSurface());
    QVERIFY(serverSurfaceCreated.wait());
    SurfaceInterface *directChild2ServerSurface = serverSurfaceCreated.last().first().value<Wrapland::Server::SurfaceInterface*>();
    QVERIFY(directChild2ServerSurface);

    // create the sub surfaces for them
    std::unique_ptr<SubSurface> directChild1SubSurface(m_subCompositor->createSubSurface(directChild1.get(), parent.get()));
    directChild1SubSurface->setMode(SubSurface::Mode::Desynchronized);
    std::unique_ptr<SubSurface> directChild2SubSurface(m_subCompositor->createSubSurface(directChild2.get(), parent.get()));
    directChild2SubSurface->setMode(SubSurface::Mode::Desynchronized);

    // each of the children gets a child
    std::unique_ptr<Surface> childFor1(m_compositor->createSurface());
    QVERIFY(serverSurfaceCreated.wait());
    SurfaceInterface *childFor1ServerSurface = serverSurfaceCreated.last().first().value<Wrapland::Server::SurfaceInterface*>();
    std::unique_ptr<Surface> childFor2(m_compositor->createSurface());
    QVERIFY(serverSurfaceCreated.wait());
    SurfaceInterface *childFor2ServerSurface = serverSurfaceCreated.last().first().value<Wrapland::Server::SurfaceInterface*>();

    // create sub surfaces for them
    std::unique_ptr<SubSurface> childFor1SubSurface(m_subCompositor->createSubSurface(childFor1.get(), directChild1.get()));
    childFor1SubSurface->setMode(SubSurface::Mode::Desynchronized);
    std::unique_ptr<SubSurface> childFor2SubSurface(m_subCompositor->createSubSurface(childFor2.get(), directChild2.get()));
    childFor2SubSurface->setMode(SubSurface::Mode::Desynchronized);

    // both get a quarter of the grand-parent surface
    childFor2SubSurface->setPosition(QPoint(50, 50));
    childFor2->commit(Surface::CommitFlag::None);
    directChild2->commit(Surface::CommitFlag::None);
    parent->commit(Surface::CommitFlag::None);

    // now let's render both grand children
    QImage partImage(QSize(50, 50), QImage::Format_ARGB32_Premultiplied);
    partImage.fill(Qt::green);
    childFor1->attachBuffer(m_shm->createBuffer(partImage));
    childFor1->damage(QRect(0, 0, 50, 50));
    childFor1->commit(Surface::CommitFlag::None);
    partImage.fill(Qt::blue);

    childFor2->attachBuffer(m_shm->createBuffer(partImage));
    // child for 2's input region is subdivided into quadrants, with input mask on the top left and bottom right
    QRegion region;
    region += QRect(0,0,25,25);
    region += QRect(25,25,25,25);
    childFor2->setInputRegion(m_compositor->createRegion(region).get());
    childFor2->damage(QRect(0, 0, 50, 50));
    childFor2->commit(Surface::CommitFlag::None);

    QSignalSpy treeChangedSpy(parentServerSurface, &SurfaceInterface::subSurfaceTreeChanged);
    QVERIFY(treeChangedSpy.isValid());
    QVERIFY(treeChangedSpy.wait());

    QCOMPARE(directChild1ServerSurface->subSurface()->parentSurface().data(), parentServerSurface);
    QCOMPARE(directChild2ServerSurface->subSurface()->parentSurface().data(), parentServerSurface);
    QCOMPARE(childFor1ServerSurface->subSurface()->parentSurface().data(), directChild1ServerSurface);
    QCOMPARE(childFor2ServerSurface->subSurface()->parentSurface().data(), directChild2ServerSurface);

    // now let's test a few positions
    QCOMPARE(parentServerSurface->surfaceAt(QPointF(0, 0)), childFor1ServerSurface);
    QCOMPARE(parentServerSurface->surfaceAt(QPointF(49, 49)), childFor1ServerSurface);
    QCOMPARE(parentServerSurface->surfaceAt(QPointF(50, 50)), childFor2ServerSurface);
    QCOMPARE(parentServerSurface->surfaceAt(QPointF(100, 100)), childFor2ServerSurface);
    QCOMPARE(parentServerSurface->surfaceAt(QPointF(100, 50)), childFor2ServerSurface);
    QCOMPARE(parentServerSurface->surfaceAt(QPointF(50, 100)), childFor2ServerSurface);
    QCOMPARE(parentServerSurface->surfaceAt(QPointF(25, 75)), parentServerSurface);
    QCOMPARE(parentServerSurface->surfaceAt(QPointF(75, 25)), parentServerSurface);

    QCOMPARE(parentServerSurface->inputSurfaceAt(QPointF(0, 0)), childFor1ServerSurface);
    QCOMPARE(parentServerSurface->inputSurfaceAt(QPointF(49, 49)), childFor1ServerSurface);
    QCOMPARE(parentServerSurface->inputSurfaceAt(QPointF(50, 50)), childFor2ServerSurface);
    QCOMPARE(parentServerSurface->inputSurfaceAt(QPointF(99, 99)), childFor2ServerSurface);
    QCOMPARE(parentServerSurface->inputSurfaceAt(QPointF(99, 50)), parentServerSurface);
    QCOMPARE(parentServerSurface->inputSurfaceAt(QPointF(50, 99)), parentServerSurface);
    QCOMPARE(parentServerSurface->inputSurfaceAt(QPointF(25, 75)), parentServerSurface);
    QCOMPARE(parentServerSurface->inputSurfaceAt(QPointF(75, 25)), parentServerSurface);

    // outside the geometries should be no surface
    QVERIFY(!parentServerSurface->surfaceAt(QPointF(-1, -1)));
    QVERIFY(!parentServerSurface->surfaceAt(QPointF(101, 101)));
}

void TestSubSurface::testDestroyAttachedBuffer()
{
    // this test verifies that destroying of a buffer attached to a sub-surface works
    using namespace Wrapland::Client;
    using namespace Wrapland::Server;
    // create surface
    QSignalSpy serverSurfaceCreated(m_compositorInterface, &CompositorInterface::surfaceCreated);
    QVERIFY(serverSurfaceCreated.isValid());
    std::unique_ptr<Surface> parent(m_compositor->createSurface());
    QVERIFY(serverSurfaceCreated.wait());
    std::unique_ptr<Surface> child(m_compositor->createSurface());
    QVERIFY(serverSurfaceCreated.wait());
    SurfaceInterface *serverChildSurface = serverSurfaceCreated.last().first().value<Wrapland::Server::SurfaceInterface*>();
    // create sub-surface
    auto *sub = m_subCompositor->createSubSurface(child.get(), parent.get());

    // let's damage this surface, will be in sub-surface pending state
    QImage image(QSize(100, 100), QImage::Format_ARGB32_Premultiplied);
    image.fill(Qt::red);
    child->attachBuffer(m_shm->createBuffer(image));
    child->damage(QRect(0, 0, 100, 100));
    child->commit(Surface::CommitFlag::None);
    m_connection->flush();

    // Let's try to destroy it
    QSignalSpy destroySpy(serverChildSurface, &QObject::destroyed);
    QVERIFY(destroySpy.isValid());
    delete m_shm;
    m_shm = nullptr;
    child.reset();
    QVERIFY(destroySpy.wait());

    delete sub;
}

void TestSubSurface::testDestroyParentSurface()
{
    // this test verifies that destroying a parent surface does not create problems
    // see BUG 389231
    using namespace Wrapland::Client;
    using namespace Wrapland::Server;
    // create surface
    QSignalSpy serverSurfaceCreated(m_compositorInterface, &CompositorInterface::surfaceCreated);
    QVERIFY(serverSurfaceCreated.isValid());
    std::unique_ptr<Surface> parent(m_compositor->createSurface());
    QVERIFY(serverSurfaceCreated.wait());
    SurfaceInterface *serverParentSurface = serverSurfaceCreated.last().first().value<Wrapland::Server::SurfaceInterface*>();
    std::unique_ptr<Surface> child(m_compositor->createSurface());
    QVERIFY(serverSurfaceCreated.wait());
    SurfaceInterface *serverChildSurface = serverSurfaceCreated.last().first().value<Wrapland::Server::SurfaceInterface*>();
    std::unique_ptr<Surface> grandChild(m_compositor->createSurface());
    QVERIFY(serverSurfaceCreated.wait());
    SurfaceInterface *serverGrandChildSurface = serverSurfaceCreated.last().first().value<Wrapland::Server::SurfaceInterface*>();
    // create sub-surface in desynchronized mode as Qt uses them
    auto sub1 = m_subCompositor->createSubSurface(child.get(), parent.get());
    sub1->setMode(SubSurface::Mode::Desynchronized);
    auto sub2 = m_subCompositor->createSubSurface(grandChild.get(), child.get());
    sub2->setMode(SubSurface::Mode::Desynchronized);

    // let's damage this surface
    // and at the same time delete the parent surface
    parent.reset();
    QSignalSpy parentDestroyedSpy(serverParentSurface, &QObject::destroyed);
    QVERIFY(parentDestroyedSpy.isValid());
    QVERIFY(parentDestroyedSpy.wait());
    QImage image(QSize(100, 100), QImage::Format_ARGB32_Premultiplied);
    image.fill(Qt::red);
    grandChild->attachBuffer(m_shm->createBuffer(image));
    grandChild->damage(QRect(0, 0, 100, 100));
    grandChild->commit(Surface::CommitFlag::None);
    QSignalSpy damagedSpy(serverGrandChildSurface, &SurfaceInterface::damaged);
    QVERIFY(damagedSpy.isValid());
    QVERIFY(damagedSpy.wait());

    // Let's try to destroy it
    QSignalSpy destroySpy(serverChildSurface, &QObject::destroyed);
    QVERIFY(destroySpy.isValid());
    child.reset();
    QVERIFY(destroySpy.wait());

    delete sub1;
    delete sub2;
}

QTEST_GUILESS_MAIN(TestSubSurface)
#include "test_wayland_subsurface.moc"
