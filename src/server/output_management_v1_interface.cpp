/********************************************************************
Copyright © 2014 Martin Gräßlin <mgraesslin@kde.org>
Copyright © 2015 Sebastian Kügler <sebas@kde.org>
Copyright © 2020 Roman Gilg <subdiff@gmail.com>

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) version 3, or any
later version accepted by the membership of KDE e.V. (or its
successor approved by the membership of KDE e.V.), which shall
act as a proxy defined in Section 6 of version 3 of the license.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library.  If not, see <http://www.gnu.org/licenses/>.
*********************************************************************/
#include "output_management_v1_interface.h"

#include "display.h"
#include "global_p.h"
#include "output_configuration_v1_interface.h"

#include "wayland-output-management-v1-server-protocol.h"
#include <wayland-server.h>

#include <QHash>

namespace Wrapland
{
namespace Server
{

class OutputManagementV1Interface::Private : public Global::Private
{
public:
    Private(OutputManagementV1Interface *q, Display *d);

private:
    void bind(wl_client *client, uint32_t version, uint32_t id) override;

    static void unbind(wl_resource *resource);
    static Private *cast(wl_resource *r) {
        return reinterpret_cast<Private*>(wl_resource_get_user_data(r));
    }
    void createConfiguration(wl_client *client, wl_resource *resource, uint32_t id);

    static void createConfigurationCallback(wl_client *client, wl_resource *resource, uint32_t id);

    OutputManagementV1Interface *q;
    static const struct zkwinft_output_management_v1_interface s_interface;
    static const quint32 s_version;

    QHash<wl_resource*, OutputConfigurationV1Interface*> configurationInterfaces;
};

const quint32 OutputManagementV1Interface::Private::s_version = 1;

const struct zkwinft_output_management_v1_interface
        OutputManagementV1Interface::Private::s_interface = {
    createConfigurationCallback
};

OutputManagementV1Interface::OutputManagementV1Interface(Display *display, QObject *parent)
: Global(new Private(this, display), parent)
{
}

OutputManagementV1Interface::~OutputManagementV1Interface()
{
}

void OutputManagementV1Interface::Private::createConfigurationCallback(wl_client *client,
                                                                       wl_resource *resource,
                                                                       uint32_t id)
{
    cast(resource)->createConfiguration(client, resource, id);
}

void OutputManagementV1Interface::Private::createConfiguration(wl_client* client,
                                                               wl_resource* resource, uint32_t id)
{
    auto config = new OutputConfigurationV1Interface(q, resource);
    config->create(display->getConnection(client), wl_resource_get_version(resource), id);
    if (!config->resource()) {
        wl_resource_post_no_memory(resource);
        delete config;
        return;
    }

    configurationInterfaces[resource] = config;
    connect(config, &QObject::destroyed, q, [this, resource] {
        configurationInterfaces.remove(resource);
    });
}

OutputManagementV1Interface::Private::Private(OutputManagementV1Interface *q, Display *d)
: Global::Private(d, &zkwinft_output_management_v1_interface, s_version)
, q(q)
{
}

void OutputManagementV1Interface::Private::bind(wl_client *client, uint32_t version, uint32_t id)
{
    auto c = display->getConnection(client);
    wl_resource *resource = c->createResource(&zkwinft_output_management_v1_interface,
                                              qMin(version, s_version), id);
    if (!resource) {
        wl_client_post_no_memory(client);
        return;
    }
    wl_resource_set_implementation(resource, &s_interface, this, unbind);
    // TODO: should we track?
}

void OutputManagementV1Interface::Private::unbind(wl_resource *resource)
{
    Q_UNUSED(resource)
    // TODO: implement?
}

}
}
