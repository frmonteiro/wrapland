stages:
  - Compliance
  - Build
  - Test
  - Integration

variables:
  IMAGE_BASE: ${CI_REGISTRY}/kwinft/ci-images/archlinux/base:latest

workflow:
  rules:
    - when: always


Message lint:
  stage: Compliance
  image: node:latest
  rules:
    - if: $CI_MERGE_REQUEST_IID
      when: always
    - if: '$CI_COMMIT_BRANCH == "master" || $CI_COMMIT_BRANCH =~ /^Plasma\// || $CI_COMMIT_TAG'
      when: never
    - when: always
  variables:
    UPSTREAM: https://${CI_REGISTRY_USER}:${CI_REGISTRY_PASSWORD}@${CI_SERVER_HOST}/kwinft/wrapland.git
  script:
    - if [ -n "$CI_MERGE_REQUEST_TARGET_BRANCH_NAME" ];
      then export COMPARE_BRANCH=$CI_MERGE_REQUEST_TARGET_BRANCH_NAME; else export COMPARE_BRANCH=master; fi
    - "echo Branch to compare: $COMPARE_BRANCH"
    - yarn global add @commitlint/cli
    - yarn add conventional-changelog-conventionalcommits
    - git remote add _upstream $UPSTREAM || git remote set-url _upstream $UPSTREAM
    - git fetch -q _upstream $COMPARE_BRANCH
    - commitlint --verbose --config=ci/commitlint.config.js --from=_upstream/$COMPARE_BRANCH
  cache:
    paths:
      - node_modules/


.common-build: &common-build
  stage: Build
  image: ${IMAGE_BASE}
  artifacts:
    paths:
      - ci-build
    expire_in: 1 week

Normal build:
  <<: *common-build
  script:
    - pacman -Sy extra-cmake-modules --noconfirm --needed
    - mkdir ci-build && cd ci-build
    - cmake -DCMAKE_INSTALL_PREFIX:PATH=/usr ../
    - make -j$(nproc)
    - make install -j$(nproc)

Sanitizers build:
  <<: *common-build
  script:
    - pacman -Sy extra-cmake-modules --noconfirm --needed
    - mkdir ci-build && cd ci-build
    - cmake -DECM_ENABLE_SANITIZERS='address;leak;undefined'
      -DCMAKE_CXX_COMPILER=clang++ ../
    - make -j$(nproc)
    - make install -j$(nproc)


.common-test: &common-test
  stage: Test
  image: ${IMAGE_BASE}
  script:
    - pacman -Sy breeze-icons --noconfirm --needed
    - cd ci-build
    - Xvfb :1 -ac -screen 0 1920x1080x24 > /dev/null 2>&1 &
    - export DISPLAY=:1
    - export WAYLAND_DEBUG=1
    - export QT_LOGGING_RULES="*=true"
    - ctest -N
    # Tests currently can only run in one thread, see issue #3.
    - dbus-run-session ctest --output-on-failure

Autotests:
  <<: *common-test
  needs:
    - job: Normal build
      artifacts: true

Sanitizers:
  <<: *common-test
  needs:
    - job: Sanitizers build
      artifacts: true


Master image trigger:
  stage: Integration
  rules:
    - if: '$CI_PROJECT_NAMESPACE == "kwinft" && $CI_COMMIT_BRANCH == "master" && $CI_PIPELINE_SOURCE == "push"'
      when: on_success
    - when: never
  variables:
    TARGET_ID: 17539846
  script: curl --request POST --form token=${CI_JOB_TOKEN} --form ref=master
            --form "variables[TRIGGER_TYPE]=wrapland_rebuild"
            ${CI_API_V4_URL}/projects/${TARGET_ID}/trigger/pipeline

Stable image trigger:
  stage: Integration
  rules:
    - if: '$CI_PROJECT_NAMESPACE == "kwinft" && $CI_COMMIT_BRANCH =~ /^Plasma\// && $CI_PIPELINE_SOURCE == "push"'
      when: on_success
    - when: never
  variables:
    TARGET_ID: 17539846
  script: curl --request POST --form token=${CI_JOB_TOKEN} --form ref=master
            --form "variables[TRIGGER_TYPE]=wrapland_rebuild"
            --form "variables[PLASMA_VERSION]=${CI_COMMIT_BRANCH}"
            ${CI_API_V4_URL}/projects/${TARGET_ID}/trigger/pipeline

Stable image tag trigger:
  stage: Integration
  rules:
    - if: '$CI_PROJECT_NAMESPACE == "kwinft" && $CI_COMMIT_TAG != null && $CI_PIPELINE_SOURCE == "push"'
      when: on_success
    - when: never
  variables:
    TARGET_ID: 17539846
  script: curl --request POST --form token=${CI_JOB_TOKEN} --form ref=master
            --form "variables[TRIGGER_TYPE]=wrapland_rebuild"
            --form "variables[WRAPLAND_TAG]=${CI_COMMIT_TAG}"
            ${CI_API_V4_URL}/projects/${TARGET_ID}/trigger/pipeline
